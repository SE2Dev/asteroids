module.exports = {
	env: {
		"browser": true,
		"es6": true,
		"node": true
	},
	plugins: ['@typescript-eslint'],
	extends: ['plugin:@typescript-eslint/recommended'],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		"ecmaVersion": 2018,
		"sourceType": "module"
	},
	globals: {
		"Atomics": "readonly",
		"SharedArrayBuffer": "readonly"
	},
	rules: {
		// note you must disable the base rule as it can report incorrect errors
		"indent": "off",
		"@typescript-eslint/indent": [
			"error",
			"tab"
		],
		"quotes": [
			"error",
			"double"
		],
		"semi": [
			"error",
			"always"
		],
		"eqeqeq": [
			"error",
			"smart"
		],
		"no-throw-literal": ["error"],
		"no-unused-expressions": [
			"error", {
				"allowShortCircuit": true,
				"allowTernary": true
			}
		],
		"no-shadow": [
			"error", {
				"builtinGlobals": true,
				"hoist": "functions",
				"allow": []
			}
		],
		// Additional TypeScript specific options
		"@typescript-eslint/class-name-casing": [
			"warn"
		]
	}
};