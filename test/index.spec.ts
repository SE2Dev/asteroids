/* eslint-disable no-unused-expressions */
import { expect } from "chai";
import { hello } from "../src/index";

// https://medium.com/@FizzyInTheHall/run-typescript-mocha-tests-in-visual-studio-code-58e62a173575

describe("Default Suite", (): void => {
	it("returns hello world", (): void => {
		expect(hello()).to.equal("Hello World!");
	});	
});